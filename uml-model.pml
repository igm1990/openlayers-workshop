@startuml
namespace domain {
    class User << (S,Aquamarine) >> {
        + ID primitive.ObjectID
        + Name string
        + Password string
        + Role string
        + Active bool

        + EncryptPassword() (User, error)
        + CheckPassword(password string) bool

    }
}


@enduml
